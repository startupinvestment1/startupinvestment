document.addEventListener('DOMContentLoaded', () => {
    const confirmLogout = document.querySelector('.yes-btn');
    const logoutModal = new bootstrap.Modal(document.getElementById('logoutModal'));

    confirmLogout.addEventListener('click', async () => {
        try {
            const response = await fetch('/logout', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.ok) {
                logoutModal.hide(); // Hide the logout modal
                showAlert('Logout successful', 'success');
            } else {
                logoutModal.hide(); // Hide the logout modal
                showAlert('Logout failed', 'error');
                console.error('Logout failed');
            }
        } catch (error) {
            logoutModal.hide(); // Hide the logout modal
            showAlert('Error logging out', 'error');
            console.error('Error logging out:', error);
        }
    });

    function showAlert(message, type) {
        Swal.fire({
            text: message,
            icon: type,
            confirmButtonText: 'OK'
        }).then((result) => {
            if (result.isConfirmed && type === 'success') {
                // Redirect the user to the landing page
                window.location.href = '/';
            }
        });
    }
});
