const prevButton = document.getElementById('prev-slide');
const nextButton = document.getElementById('next-slide');
const slider = document.querySelector('.image-list');
const slides = document.querySelectorAll('.slide');

let currentIndex = 0;
const slideWidth = slides[0].clientWidth;
const totalSlides = slides.length;
let isTransitioning = false; // Flag to track transition state

prevButton.addEventListener('click', () => {
    if (!isTransitioning && currentIndex > 0) {
        currentIndex--;
        slider.style.transition = 'transform 0.5s ease'; // Adjust transition duration as needed
        slider.style.transform = `translateX(-${currentIndex * slideWidth}px)`;
        isTransitioning = true;
    }
});

nextButton.addEventListener('click', () => {
    if (!isTransitioning && currentIndex < totalSlides - 1) {
        currentIndex++;
        slider.style.transition = 'transform 0.5s ease'; // Adjust transition duration as needed
        slider.style.transform = `translateX(-${currentIndex * slideWidth}px)`;
        isTransitioning = true;
    } else if (currentIndex === totalSlides - 1) {
        // If at the last slide, stop transition without moving further
        slider.style.transition = 'none';
        setTimeout(() => {
            slider.style.transition = '';
        }, 50); // Delay to allow disabling transition
    }
});

slider.addEventListener('transitionend', () => {
    if (currentIndex === totalSlides - 1) {
        // If at the last slide, stop transition without moving further
        slider.style.transition = 'none';
        setTimeout(() => {
            slider.style.transition = '';
        }, 50); // Delay to allow disabling transition
    }
    isTransitioning = false;
});
