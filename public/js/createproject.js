document.getElementById("createProjectForm").addEventListener("submit", async function (event) {
    event.preventDefault();

    const formData = new FormData(this);
    const endDate = formData.get('endDate'); // Assuming your form has an 'endDate' input field

    // Check if the end date is in the past
    if (new Date(endDate) < new Date()) {
        // Use SweetAlert to show error message
        Swal.fire({
            text: 'End date cannot be in the past. Please select a future date.',
            icon: 'error',
            confirmButtonText: 'OK'
        });
        return; // Stop form submission
    }

    try {
        const response = await fetch('/create-project', {
            method: 'POST',
            body: formData,
            credentials: 'same-origin' // Ensures cookies are sent with the request if using sessions
        });

        if (!response.ok) {
            const errorText = await response.text();
            throw new Error(errorText || 'Failed to create project');
        }

        const data = await response.json();

        // Use SweetAlert to show success message
        Swal.fire({
            text: data.message,
            icon: 'success',
            confirmButtonText: 'OK'
        });

    } catch (error) {
        console.error(error);

        // Use SweetAlert to show error message
        Swal.fire({
            text: error.message || 'Error creating project',
            icon: 'error',
            confirmButtonText: 'Try Again'
        });
    }
});