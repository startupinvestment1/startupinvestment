document.getElementById('notification-icon').addEventListener('click', async () => {
    const popup = document.getElementById('notification-popup');
    const list = document.getElementById('notifications-list');

    // Toggle the display of the popup
    popup.style.display = popup.style.display === 'none' ? 'block' : 'none';

    // Fetch notifications if the popup is being shown
    if (popup.style.display === 'block') {
        try {
            const response = await fetch('/notifications');
            if (!response.ok) {
                throw new Error('Failed to fetch notifications');
            }
            const notifications = await response.json();

            // Clear the current list of notifications
            list.innerHTML = '';

            // Populate the list with notifications (latest at the top)
            notifications.reverse().forEach(notification => {
                const listItem = document.createElement('div');
                listItem.className = `notification-item ${notification.type}`; // Assuming notifications have a "type" property
                listItem.innerHTML = `<p>${notification.message}</p>`; // Assuming notifications have a "message" property
                list.appendChild(listItem);
            });

            // Mark notifications as read
            await markNotificationsAsRead(notifications);

            // Clear the badge count
            const badge = document.getElementById('notification-badge');
            badge.style.display = 'none';
            badge.textContent = '';

        } catch (error) {
            console.error('Error fetching notifications:', error);
            list.innerHTML = '<div class="notification-item error"><p>Error loading notifications</p></div>';
        }
    }
});

// Function to mark notifications as read
async function markNotificationsAsRead(notifications) {
    const unreadNotifications = notifications.filter(notification => !notification.isRead);

    if (unreadNotifications.length > 0) {
        try {
            const response = await fetch('/notifications/markAsRead', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ notifications: unreadNotifications.map(n => n._id) })
            });

            if (!response.ok) {
                throw new Error('Failed to mark notifications as read');
            }
        } catch (error) {
            console.error('Error marking notifications as read:', error);
        }
    }
}

// Fetch notifications periodically to update the badge count
async function updateNotificationBadge() {
    try {
        const response = await fetch('/notifications');
        if (!response.ok) {
            throw new Error('Failed to fetch notifications');
        }
        const notifications = await response.json();

        // Update the badge count with the number of unread notifications
        const unreadCount = notifications.filter(notification => !notification.isRead).length;
        const badge = document.getElementById('notification-badge');
        if (unreadCount > 0) {
            badge.textContent = unreadCount;
            badge.style.display = 'block';
        } else {
            badge.style.display = 'none';
        }
    } catch (error) {
        console.error('Error fetching notifications:', error);
    }
}

// Call updateNotificationBadge on page load and set an interval to refresh it periodically
document.addEventListener('DOMContentLoaded', () => {
    updateNotificationBadge();
    setInterval(updateNotificationBadge, 60000); // Update every 60 seconds
});

// Hide the popup if the user clicks outside of it
document.addEventListener('click', (event) => {
    const popup = document.getElementById('notification-popup');
    if (!document.getElementById('notification-icon').contains(event.target)) {
        popup.style.display = 'none';
    }
});
